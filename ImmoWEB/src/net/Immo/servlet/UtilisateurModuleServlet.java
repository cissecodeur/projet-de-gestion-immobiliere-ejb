package net.Immo.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import net.immo.entities.Compte;
import net.immo.entities.Groupe;
import net.immo.entities.Logement;
import net.immo.entities.Profil;
import net.immo.entities.Quartier;
import net.immo.entities.TypeClient;
import net.immo.entities.TypeLogement;
import net.immo.entities.Utilisateur;
import net.immo.interfaces.CompteI;
import net.immo.interfaces.GroupeI;
import net.immo.interfaces.ProfilI;
import net.immo.interfaces.TypeClientI;
import net.immo.interfaces.UtilisateurI;

/**
 * Servlet implementation class UtilisateurModuleServlet
 */
@WebServlet("/UtilisateurModuleServlet")
public class UtilisateurModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	
	@EJB
	
	private UtilisateurI MetierUtilisateur;
	
	@EJB
	
	private CompteI MetierCompte;
	
	@EJB
	private ProfilI MetierProfil;
	
	@EJB
	private GroupeI MetierGroupe;
	
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UtilisateurModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		 String action=request.getParameter("action");
		  String json="";
		  
		  
		     if(action !=null){
		    	 
		    	 if(action.equals("addNewUtilisateur")){   
		    		     		 
		    		 int idCompte=Integer.parseInt(request.getParameter("idCompte"));
		    		 String nomUtilisateur=request.getParameter("nomUtilisateur");
		    		 int idProfil=Integer.parseInt(request.getParameter("idProfil"));
		    		 int idGroupe=Integer.parseInt(request.getParameter("idGroupe"));
		    		 String prenomUtilisateur=request.getParameter("prenomUtilisateur");
		    		 String emailUtilisateur=request.getParameter("emailUtilisateur");
		    		 String telephoneUtilisateur=request.getParameter("telephoneUtilisateur");
		    		 String fonctionUtlisateur=request.getParameter("fonctionUtlisateur");
		    		 String passwordUtilisateur=request.getParameter("passwordUtilisateur");
		    		 int  etatUtilisateur=Integer.parseInt(request.getParameter(" etatUtilisateu"));
		    		 
		    		 
		    		//Je cree une nouvelle instance de Compte apr�s avoir injecter sa dependance   		       		 
		    		Compte c=new Compte();    		 
		    		 c.setIdCompte(idCompte);		    		
		    		 		    		 
		    		 //Je cree une nouvelle instance de Profil apr�s avoir injecter sa dependance     		    
		    		 Profil p=new Profil();
		    		 p.setIdProfil(idProfil);
		    		 
		    		 //Je cree une nouvelle instance de Profil apr�s avoir injecter sa dependance     		    
		    		Groupe g=new Groupe();
		    		 g.setIdGroupe(idGroupe);
		    		 
		    		 Utilisateur u= new Utilisateur();
		    		 
		    		 u.setCompte(c);
		    		 u.setNomUtilisateur(nomUtilisateur);
		    		 u.setProfil(p);
		    		 u.setGroupe(g);
		    		 u.setPrenomUtilisateur(prenomUtilisateur);
		    		 u.setEmailUtilisateur(emailUtilisateur);
		    		 u.setTelephoneUtilisateur(telephoneUtilisateur);
		    		 u.setTelephoneUtilisateur(telephoneUtilisateur);
		    		 u.setFonctionUtlisateur(fonctionUtlisateur);
		    		 u.setPasswordUtilisateur(passwordUtilisateur);
					 u.setEtatUtilisateur(etatUtilisateur);
		    		 

		    		 
		    		 
		    			    		
		    			    		
		    		 Utilisateur retour= MetierUtilisateur.addUtilisateur(u);
		    		
		    		
		    		
		    		json =new Gson().toJson(retour);
		    	 }
		    	 else if(action.equals("")){   
		     		 
		    		 
		         }
		     }
		     
		     response.setContentType("application/json");
		     response.getWriter().write(json);
				
		    		 
		
		
		
		
		
	}

}
