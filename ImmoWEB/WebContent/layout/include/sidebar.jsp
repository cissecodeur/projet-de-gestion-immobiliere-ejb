<div class="menu_section">
  <h3>General</h3>
  <ul class="nav side-menu">
  <li><a><i class="fa fa-home"></i> Tableau de bord </a>
  </li>

  <li><a><i class="fa fa-edit"></i> Log�ments <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">
  <li><a href="<%=request.getContextPath() %>/layout/logement/NouveauLogement.jsp">Nouveau</a>
  </li>
  <li><a href="<%=request.getContextPath() %>/layout/logement/ListeLogement.jsp">Liste de Log�ments</a>
  </li>
  </ul>
  </li>
  
  
  

  <li><a><i class="fa fa-bar-chart-o"></i> Statistiques <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">
  <li><a href="layout/statistiques/chartjs.html">Chart JS</a>
  </li>
  <li><a href="layout/statistiques/chartjs2.html">Chart JS2</a>
  </li>
  <li><a href="layout/statistiques/morisjs.html">Moris JS</a>
  </li>
  <li><a href="layout/statistiques/echarts.html">ECharts </a>
  </li>
  </ul>
  </li>


  <li><a><i class="fa fa-desktop"></i> Administration <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">

  <li><a><i class="glyphicon glyphicon-bullhorn"></i> Gestion comptes <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">
  
   <li><a href="<%=request.getContextPath() %>/layout/administration/NouveautypeClient.jsp">Cr�er Type de clients</a></li>
   <li><a href="<%=request.getContextPath() %>/layout/administration/ListeTypeClient.jsp">Liste type clients</a></li>
   
   <li><a href="<%=request.getContextPath() %>/layout/administration/NouveauClient.jsp">Cr�er clients</a></li>
  <li><a href="<%=request.getContextPath() %>/layout/administration/ListeClient.jsp">Liste clients</a></li>
  <li><a href="general_elements.html">Flotte log�ments clients</a></li>
  </ul>
  </li>

  <li><a><i class="glyphicon glyphicon-home"></i> Gestion log�ments <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">
  <li><a href="<%=request.getContextPath() %>/layout/administration/NouveauLogement.jsp">Cr�er log�ments</a></li>
  <li><a href="<%=request.getContextPath() %>/layout/administration/ListeLogement.jsp">Liste log�ments</a></li>
  </ul>
  </li>

  <li><a><i class="glyphicon glyphicon-user"></i> Gestion utilisateur <span class="fa fa-chevron-down"></span></a>
  <ul class="nav child_menu" style="display: none">
  <li><a href="general_elements.html">Utilisateurs</a></li>
   
   <li><a href="<%=request.getContextPath() %>/layout/administration/NouveauUtilisateur.jsp">Cr�er utilisateur</a></li>
  <li><a href="general_elements.html">Liste clients</a></li>
  
  <li><a href="general_elements.html">Types utilisateurs</a></li>
  </ul>
  </li>


  </ul>
  </li>
  <li><a><i class="fa fa-table"></i> A propos </a>
  </li>

  </ul>
  </div>