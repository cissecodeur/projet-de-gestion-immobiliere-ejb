package net.Immo.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import net.immo.entities.Logement;
import net.immo.entities.Quartier;
import net.immo.entities.TypeLogement;
import net.immo.interfaces.LogementI;
import net.immo.interfaces.QuartierI;
import net.immo.interfaces.TypeLogementI;



public class LogementModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    	
	

	//Injection de dependances
	
	@EJB
	private LogementI MetierLogement;
	
	@EJB
	private TypeLogementI MetierTypeLogement;
	
	@EJB
	private QuartierI MetierQuatier;
	
	
	
//Constructeurs sans paramettres
	
    public LogementModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	
	
	
	
	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  String action=request.getParameter("action");
  String json="";
  
  
     if(action !=null){
    	 
    	 if(action.equals("addNewLogement")){   
    		     		 
    		 int idTypeLogement=Integer.parseInt(request.getParameter("idTypeLogement"));
    		 String libelleLogement=request.getParameter("libelleLogement");
    		 int idQuartier=Integer.parseInt(request.getParameter("idQuartier"));
    		 String addresseLogement=request.getParameter("addresseLogement");
    		 String Superficie=request.getParameter("Superficie");
    		 int nombredePiece=Integer.parseInt(request.getParameter("nombredePiece"));
    		 
    		 
    		//Je cree une nouvelle instance de TypeLogement apr�s avoir injecter sa dependance   		       		 
    		 TypeLogement t=new TypeLogement();    		 
    		 t.setIdTypeLogement(idTypeLogement);
    		 
    		 
    		 //Je cree une nouvelle instance de Quatier apr�s avoir injecter sa dependance     		    
    		 Quartier q=new Quartier();
    		 q.setIdQuartier(idQuartier);
    		 
    		 
    		 Logement l= new Logement();
    		 
    		 l.setTypelogement(t);
    		 l.setLibelleLogement(libelleLogement);
    		 l.setQuartier(q);
    		 l.setAddresseLogement(addresseLogement);
    		 l.setSuperficieLogement(Superficie);
    		 l.setNombredepieces(nombredePiece);
    		
    		Logement retour= MetierLogement.addLogement(l);
    		
    		json =new Gson().toJson(retour);
    		
    	 }
    	 else if(action.equals("getListLogement")){   
     		 
    		 List<Logement> retour= MetierLogement.listLogement();
    		 json =new Gson().toJson(retour);
    		 
         }else if(action.equals("deleteLogement")){   
     		 
        	 int idLogement=Integer.parseInt(request.getParameter("idLogement"));
        	 
        	 Logement retour=MetierLogement.SupprimerLogement(idLogement);
        	 
    		 json =new Gson().toJson(retour);
         }
     }
     
     response.setContentType("application/json");
     response.getWriter().write(json);
		
	}

}
