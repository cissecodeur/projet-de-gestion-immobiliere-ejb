var ROOT_URL='http://localhost:8080/ImmoWEB';

function formulaireNouveautypeClient(){
	
	var dDiv='';
	 dDiv +='<form id="demo-form" data-parsley-validate onsubmit ="return false">'+
     
	
	 '<div class="row">'+
		'<div class="col-md-6">'+
			'  <label >Libell&eacute; :</label>'+
			'   <input class="form-control"  id="libelleTypeClient"/>'+
		'</div>'+	
	'</div>'+	
			
    '<div class="row">'+
	' <div class="col-md-6">'+
	'     <span class="btn btn-primary" onclick="addNewtypeClient()">Valider</span>'+
	' </div>'+
 ' </div>'+
 ' </div>'+

	

	'</form>';
     
	$('#FormulaireNouveautypeClient').empty();
	$('#FormulaireNouveautypeClient').append(dDiv);
}





function addNewtypeClient(){

    var libelleTypeClient= $("#libelleTypeClient").val();
    
    
    swal({
        title: "Voulez-vous vraiment ajouter ce type de client ?",
        text: "Cliquez sur valider pour confirmer.",
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function () {
       setTimeout(function () {
    	
    var key="addNewtypeClient";
	var DTO={"action":key,"libelleTypeClient":libelleTypeClient}
	
    
// Bloc ajax
	
    $.ajax({

        type: "POST",
        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        url:ROOT_URL+"/TypeClientModuleServlet",
        dataType: "json",
        data: DTO,
        
        success: function (r) { 
        	           
           
        	if(r.idTypeclient>0){		          
   	    	 
                swal("Succès!", "Type de client ajouté avec succès!", "success");
                document.location.reload(true);

            }else{
                swal("Erreur...", "Impossible d'ajouter ce type de client!\n Actualiser la page puis reessayer!!! ", "error");
            }
        	
        },
        error: function (r) {
        	 swal("Erreur...", "Impossible d'executer la requette \n  Erreur: "+ r, "error");
        }
    });

       }, null);
    });

    return true;
    }



$(document).ready(function () {
	
	formulaireNouveautypeClient();
	
	
});