var ROOT_URL='http://localhost:8080/ImmoWEB';


function getListClient(){
	
	  var dListUser = '<table id="datatable-fixed-header" class="table table-striped table-bordered">';
	  var listDemande = null;
	  var dListUserRow = '';
		
	
	  var key="getListClient";
	  var DTO={"action":key}
	  
	  
	 $.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/ClientModuleServlet",
	        dataType: "json",
	        data: DTO,
       success: function (r) {
    	       	
    	  listDemande=r;
    	  
    	  console.log(JSON.stringify(r));
    	 $.each(listDemande, function (i, item) { 
    		  var actions="";   		 
    		 
    		
    		  
    		 dListUserRow += '<tr>\n' +
    		 
           '<td class="text-center">\n'
      	 + (i+1) +
           '</td>\n' +
           
           '<td>\n'
           + item.typeClient.libelleTypeClient +
           '</td>\n' +
           
           '<td>\n'
           + item.raisonSocialClient+
           '</td>\n' +
           
           '<td>\n'
           + item.numCompteContribuableClient +
           '</td>\n' +
           
           '<td>\n'
           + item.nomClient+
           '</td>\n' +
           
           
           '<td>\n'
           + item.prenomClient+
           '</td>\n' +
           
           '<td>\n'
           + item.contactClient+
           '</td>\n' +
           
           '<td>\n'
           + item.addresseClient+
           '</td>\n' +
           
           '<td>\n'
           + item.activiteClient+
           '</td>\n' +
        
           
           '<td class="text-center">\n';   		           
          
    		 
    		  dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="modal" data-target="#modal_large" onclick="chargerClientById('+item.idClient+')" title="Modifier ce client"><span class="glyphicon glyphicon-pencil"></span></button>';
         	   dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" onclick=SupprimerClient('+item.idClient+') title="Supprimer ce client"><span class="fa fa-trash"></span></button>';
               

    		 
    		 
    		 
           dListUserRow +=  actions+
          '</td>\n' 
           
          + '</tr>\n';
    		 
       }); 
    	 
    	 dListUser += dListUserRow;
       dListUser += ' </tbody><thead><tr>' +
       '<th width="5%"   class="text-center">Numero </th>' +
       '<th width="15%"  class="text-center">Type</th>' +
       '<th width="15%"  class="text-center">Raison Sociale</th>' +
       '<th width="10%"  class="text-center">Numero de Compte</th>' +
       '<th width="15%"  class="text-center">Nom</th>' +
       '<th width="20%"  class="text-center">Prenoms</th>' +
       '<th width="10%"  class="text-center">Contact</th>' +
       '<th width="20%"  class="text-center">Addresse</th>' +
       '<th width="20%"  class="text-center">Activit&eacute;</th>' +
       '<th width="10%"  class="text-center">Actions</th>' +
       '</tr></thead></table>';
       
       
       
       
       $('#listeDemande').empty();
       $('#listeDemande').append(dListUser);
   
       
       var tableCourse = $('#datatable-fixed-header').dataTable({        	 
      	           
      	 
       	 //gerer la langue
	       "oLanguage": {
	            "sLengthMenu": "_MENU_ / pages",
	            "sZeroRecords": "Aucun resultat trouv&eacute;",
	            "sInfo": "_START_ a _END_ sur _TOTAL_ client(s)",
	            "sInfoEmpty": "De 0 a 0 sur 0 client(s)",
	            "sInfoFiltered": "(total de _MAX_  client(s))",
	            "sSearch": "Recherche:",
	            "oPaginate": {
	                "sFirst":    "Prem",
	                "sLast":    "Dern",
	                "sNext":    "Suiv",
	                "sPrevious": "Pr&eacute;c"
	            }
	        },

	       "bFilter": true, //enlever la recherche automatique
         "bSort": false,    //enlever les filtre
         "bLengthChange": true,//enlever la selection de plusieurs enregistrements
         "aLengthMenu": [[10,25, 50, 75, -1], [25, 50, 75, "Tout"]] //mettre le nombre d'enregitrement afficher par page
       });
    	 
     },
		error: function (data, status, er) {
			 alert("Insertion échouée"+status)	;
	    }
	});
}



function SupprimerClient(idClient){
	
	
	
	 swal({
	        title: "Voulez-vous vraiment supprimer ce client ?",
	        text: "Cliquez sur valider pour confirmer.",
	        type: "warning",
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Valider",
	        cancelButtonText: "Annuler",
	        showCancelButton: true,
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	    },
	    function () {
	       setTimeout(function () {
	
	 var key="deleteClient";
	  var DTO={"action":key, 'idClient':idClient}
	
$.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/ClientModuleServlet",
	        dataType: "json",
	        data: DTO,
      success: function (r) {
   	       	
   	   swal("Succes!", "Client supprimé avec succès!", "success");
          document.location.reload(true);
   	  
   	
   		  
        },
		error: function (data, status, er) {
			 alert("Suppression échouée"+status)	;
	    }
	});
	
	       }, null);
	    });

	    return true;
}


function chargerClientById(idClient){
	
	
	 var key="chargerClientById";
	  var DTO={"action":key,'idClient':idClient}
	  
	  
	 $.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/ClientModuleServlet",
	        dataType: "json",
	        data: DTO,
      success: function (r) {
	
    	  console.log(r);
    	  FormUpdateClient(r);
      },
		error: function (data, status, er) {
			 alert("Insertion échouée"+status)	;
	    }
	});
	
}

function FormUpdateClient(r){
	
	
	
	var dDiv='';
	 dDiv +='<form id="demo-form" data-parsley-validate onsubmit ="return false">'+
    
	'<div class="row">'+
	'<div class="col-md-6">'+
		'<label for="heard">Type :</label>'+
		' <select  class="form-control" id="idTypeClient">'+
		' <option value="">Choose..</option>'+
		' <option value="1">Press</option>'+
		' <option value="2">Internet</option>'+
		' <option value="3">Word of mouth</option>'+
		'</select>'+
	'</div>'+
	'</div>'+
	
	
	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Raison Sociale :</label>'+
	'  <input type="text" class="form-control" id="RaisonSocialClient" value="'+r.raisonSocialClient+'" />'+
	'</div>   	'+
	'<div class="col-md-6">'+
	'  <label for="fullname">Numero de Compte:</label>'+
	'  <input type="text" class="form-control" id="NumCompteContribuableClient" value="'+r.numCompteContribuableClient+'" />'+
	'</div>'+
	'</div>'+
	'</div>'+
	
 '<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Nom :</label>'+
	'   <input class="form-control"  id="nomClient" value="'+r.nomClient+'" />'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label >Prenoms:</label>'+
	'   <input class="form-control"  id="prenomClient"  value="'+r.prenomClient+'"/>'+
	'</div>'+	
	'</div>'+
	'</div>'+
	

	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Contact :</label>'+
	'   <input class="form-control"  id="contactClient" value="'+r.contactClient+'"/>'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Addresse :</label>'+
	'   <input type="text"  class="form-control" id="addresseClient" value="'+r.addresseClient+'"  />'+
	'</div>'+
	'</div>'+
	'</div>'+
	
	'<div class="row">'+
	'<div class="col-md-12">'+
	'  <label for="fullname">Activit&eacute; :</label>'+
	'  <input type="text" class="form-control" id="ActiviteClient" value="'+r.activiteClient+'"/>'+
	'</div>'+
	'</div>'+

 	
 	
 	                           
	' <div class="col-md-6">'+
	'     <span class="btn btn-primary" onclick="modifierClient('+r.idClient+')">Modifier</span>'+
	' </div>'+


	'</form>';
	
	 modalUpdateClient('Modifier ce client',dDiv);
	
}


function modalUpdateClient(Title, Content){
	
    var modal = "";
	    modal += '<div id="sssss"></div>';
    modal += '<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    modal += '<h4 class="modal-title" id="largeModalHead">' + Title + '</h4></div> <div class="modal-body">';
    modal += '' + Content + '';
    modal += '</div><div class="modal-footer">';   

    /*modal += '<button type="button" class="btn btn-success" onclick="creerBonLivraison('+idDemande+')" >Créer un bon de livraison</button>';
    */
    modal += '</div>  </div> </div></div>';

    $('#modal_large').empty();
    $('#modal_large').append(modal);
    

    
    
}



	
	function UpdateClient(c){

		
		var idTypeClient = $("#idTypeClient").val();
	    var nomClient= $("#nomClient").val();
	    var prenomClient= $("#prenomClient").val();
	    var contactClient= $("#contactClient").val();
	    var addresseClient= $("#addresseClient").val();
	    var ActiviteClient= $("#ActiviteClient").val();
	    var RaisonSocialClient= $("#RaisonSocialClient").val();
	    var NumCompteContribuableClient= $("#NumCompteContribuableClient").val();
	    
	    
	    swal({
	        title: "Voulez-vous vraiment ajouter ce client ?",
	        text: "Cliquez sur valider pour confirmer.",
	        type: "warning",
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Valider",
	        cancelButtonText: "Annuler",
	        showCancelButton: true,
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	    },
	    function () {
	       setTimeout(function () {
	    	
	    var key="UpdateClient";
		var DTO={"action":key, "idTypeClient":idTypeClient,"nomClient":nomClient,"prenomClient":prenomClient,"contactClient":contactClient,"addresseClient":addresseClient,"ActiviteClient":ActiviteClient,"RaisonSocialClient":RaisonSocialClient,"NumCompteContribuableClient":NumCompteContribuableClient}
		
	    
	// Bloc ajax
		
	    $.ajax({

	        type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/ClientModuleServlet",
	        dataType: "json",
	        data: DTO,
	        
	        success: function (r) { 
	        	           
	           
	        	if(r.idClient>0){		          
	   	    	 
	                swal("Succès!", "Client modifié avec succès!", "success");
	                document.location.reload(true);

	            }else{
	                swal("Erreur...", "Impossible de modifier ce client!\n Actualiser la page puis reessayer!!! ", "error");
	            }
	        	
	        },
	        error: function (r) {
	        	 swal("Erreur...", "Impossible d'executer la requette \n  Erreur: "+ r, "error");
	        }
	    });

	       }, null);
	    });

	    return true;
	    }




$(document).ready(function () {
		
	getListClient();
	
	
});