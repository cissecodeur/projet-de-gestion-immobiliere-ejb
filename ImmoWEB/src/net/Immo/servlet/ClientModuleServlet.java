package net.Immo.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import net.immo.entities.Client;
import net.immo.entities.Logement;
import net.immo.entities.TypeClient;
import net.immo.interfaces.ClientI;
import net.immo.interfaces.TypeClientI;


/**
 * Servlet implementation class ClientModuleServlet
 */
@WebServlet("/ClientModuleServlet")
public class ClientModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	
	//Injection de dependances
	
		@EJB
		private ClientI MetierClient;
		
		@EJB
		private TypeClientI MetierTypeClient;
		
	
	
	
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		 String action=request.getParameter("action");
		  String json="";
		  
		  
		     if(action !=null){
		    	 
		    	 if(action.equals("addNewClient")){   
		    		     		 
		    		 int idTypeClient=Integer.parseInt(request.getParameter("idTypeClient"));
		    		 String nomClient=request.getParameter("nomClient");
		    		 String prenomClient=request.getParameter("prenomClient");
		    		 String contactClient=request.getParameter("contactClient");
		    		 String addresseClient=request.getParameter("addresseClient");
		    		 String ActiviteClient=request.getParameter("ActiviteClient");
		    		 String RaisonSocialClient=request.getParameter("RaisonSocialClient");
		    		 String NumCompteContribuableClient=request.getParameter("NumCompteContribuableClient");
		    		 
		    		 
		    		//Je cree une nouvelle instance de TypeClient apr�s avoir injecter sa dependance   
		    		 
		    		 TypeClient tc=new TypeClient();    		 
		    		 tc.setIdTypeclient(idTypeClient);
		    		 
		    		 
		   
		    		 
		    		 Client c= new Client();
		    	
		    		 c.setTypeClient(tc);
		    		 c.setNomClient(nomClient);
		    		
		    		 c.setPrenomClient(prenomClient);
		    		 c.setContactClient(contactClient);
		    		 c.setAddresseClient(addresseClient);
		    		 c.setActiviteClient(ActiviteClient);
		    		 c.setRaisonSocialClient(RaisonSocialClient);
		    		 c.setNumCompteContribuableClient(NumCompteContribuableClient);
		    		
		    		 Client retour= MetierClient.addClient(c);
		    		
		    		json =new Gson().toJson(retour);
		    	 }
		    	 else if(action.equals("getListClient")){  
		    		 
		    		 List<Client> retour= MetierClient.listClient();
		    		 json =new Gson().toJson(retour);	 
		    		 
		         }else if(action.equals("deleteClient")){   
		     		 
		        	 int idClient=Integer.parseInt(request.getParameter("idClient"));
		        	 
		        	 Client retour=MetierClient.SupprimerClient (idClient);
		        	 
		    		 json =new Gson().toJson(retour);
		         }
		    	 
                     else if(action.equals("chargerClientById")){   
		     		 
		        	 int idClient=Integer.parseInt(request.getParameter("idClient"));
		        	 
		        	 Client retour=MetierClient.getClientById(idClient);
		        	 
		    		 json =new Gson().toJson(retour);
		    	 
		                    }  
                              else if(action.equals("UpdateClient")){   
		   		     		 
		   		        	  
                            	  int idTypeClient=Integer.parseInt(request.getParameter("idTypeClient"));
             		    		 String nomClient=request.getParameter("nomClient");
             		    		 String prenomClient=request.getParameter("prenomClient");
             		    		 String contactClient=request.getParameter("contactClient");
             		    		 String addresseClient=request.getParameter("addresseClient");
             		    		 String ActiviteClient=request.getParameter("ActiviteClient");
             		    		 String RaisonSocialClient=request.getParameter("RaisonSocialClient");
             		    		 String NumCompteContribuableClient=request.getParameter("NumCompteContribuableClient");
             		    		 
             		    		 
             		    		//Je cree une nouvelle instance de TypeClient apr�s avoir injecter sa dependance   
             		    		 
             		    		 TypeClient tc=new TypeClient();    		 
             		    		 tc.setIdTypeclient(idTypeClient);
             		    		 
             		    		 
             		   
             		    		 
             		    		 Client c= new Client();
             		    	
             		    		 c.setTypeClient(tc);
             		    		 c.setNomClient(nomClient);
             		    		
             		    		 c.setPrenomClient(prenomClient);
             		    		 c.setContactClient(contactClient);
             		    		 c.setAddresseClient(addresseClient);
             		    		 c.setActiviteClient(ActiviteClient);
             		    		 c.setRaisonSocialClient(RaisonSocialClient);
             		    		 c.setNumCompteContribuableClient(NumCompteContribuableClient);
             		    		
             		    		 Client retour= MetierClient.UpdateClient(c);
             		    		
             		    		json =new Gson().toJson(retour);
                            	  
		   		    	 
		   		                    }
		   		     
		     
		     response.setContentType("application/json");
		     response.getWriter().write(json);
				
		    		 
		
		
	}
	
	}
		     		     

}
