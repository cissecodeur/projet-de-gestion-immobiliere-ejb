var ROOT_URL='http://localhost:8080/ImmoWEB';


function getListTypeClient(){
	
	  var dListUser = '<table id="datatable-fixed-header" class="table table-striped table-bordered">';
	  var listDemande = null;
	  var dListUserRow = '';
		
	
	  var key="getListTypeClient";
	  var DTO={"action":key}
	  
	  
	 $.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/TypeClientModuleServlet",
	        dataType: "json",
	        data: DTO,
       success: function (r) {
    	       	
    	  listDemande=r;
    	  
    	  //console.log(JSON.stringify(r));
    	 $.each(listDemande, function (i, item) { 
    		  var actions="";   		 
    		 
    		
    		  
    		 dListUserRow += '<tr>\n' +
    		 
           '<td class="text-center">\n'
      	 + (i+1) +
           '</td>\n' +
           
         
           
           '<td>\n'
           + item.libelleTypeClient+
           '</td>\n' +
           
         
           
        
           
           '<td class="text-center">\n';   	
    		 
    		 
    		 
    		 
     		dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="modal" data-target="#modal_large" onclick=ModifierLogement('+item.idTypeclient+') title="Modifier cette fourniture"><span class="glyphicon glyphicon-pencil"></span></button>';
       	   dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="modal" data-target="#modal_large" onclick=SupprimerTypeClient('+item.idTypeclient+') title="Supprimer ce type de client"><span class="fa fa-trash"></span></button>';
             
     		 
          
           dListUserRow +=  actions+
          '</td>\n' 
           
          + '</tr>\n';
    		 
       }); 
    	 
    	 dListUser += dListUserRow;
       dListUser += ' </tbody><thead><tr>' +
       '<th width="5%"   class="text-center">Numero </th>' +
       
       '<th width="20%"  class="text-center">Libell&eacute;</th>' +
       '<th width="25%"  class="text-center">Actions</th>' +
       '</tr></thead></table>';
       
       
       
       
       $('#listeDemande').empty();
       $('#listeDemande').append(dListUser);
   
       
       var tableCourse = $('#datatable-fixed-header').dataTable({        	 
      	           
      	 
       	 //gerer la langue
	       "oLanguage": {
	            "sLengthMenu": "_MENU_ / pages",
	            "sZeroRecords": "Aucun resultat trouv&eacute;",
	            "sInfo": "_START_ a _END_ sur _TOTAL_ type client(s)",
	            "sInfoEmpty": "De 0 a 0 sur 0 type client(s)",
	            "sInfoFiltered": "(total de _MAX_  type client(s))",
	            "sSearch": "Recherche:",
	            "oPaginate": {
	                "sFirst":    "Prem",
	                "sLast":    "Dern",
	                "sNext":    "Suiv",
	                "sPrevious": "Pr&eacute;c"
	            }
	        },

	       "bFilter": true, //enlever la recherche automatique
         "bSort": false,    //enlever les filtre
         "bLengthChange": true,//enlever la selection de plusieurs enregistrements
         "aLengthMenu": [[10,25, 50, 75, -1], [25, 50, 75, "Tout"]] //mettre le nombre d'enregitrement afficher par page
       });
    	 
     },
		error: function (data, status, er) {
			 alert("Insertion échouée"+status)	;
	    }
	});
}




function SupprimerTypeClient(idTypeclient){
	
		//alert(idTypeclient);
	
	swal({
        title: "Voulez-vous vraiment supprimer ce type de client ?",
        text: "Cliquez sur valider pour confirmer.",
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function () {
       setTimeout(function () {

 var key="deleteTypeClient";
  var DTO={"action":key, 'idTypeClient':idTypeclient}

 // alert(JSON.stringify(DTO));
  
  $.ajax({      
	 
	 type: "POST",
        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        url:ROOT_URL+"/TypeClientModuleServlet",
        dataType: "json",
        data: DTO,
        success: function (r) {
	       	
        swal("Succes!", "Type de client supprimé avec succès!", "success");
	   	document.location.reload(true);
	  
	
		  
	   },
		error: function (data, status, er) {
			 alert("Suppression échouée"+status)	;
	    }
	});

       }, null);
    });

    
	 
}





$(document).ready(function () {
		
	getListTypeClient();
	
	
});