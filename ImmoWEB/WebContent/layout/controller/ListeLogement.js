var ROOT_URL='http://localhost:8080/ImmoWEB';


function getListLogement(){
	
	  var dListUser = '<table id="datatable-fixed-header" class="table table-striped table-bordered">';
	  var listDemande = null;
	  var dListUserRow = '';
		
	
	  var key="getListLogement";
	  var DTO={"action":key}
	  
	  
	 $.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/LogementModuleServlet",
	        dataType: "json",
	        data: DTO,
       success: function (r) {
    	       	
    	  listDemande=r;
    	  
    	  console.log(JSON.stringify(r));
    	 $.each(listDemande, function (i, item) { 
    		  var actions="";   		 
    		 
    		
    		  
    		 dListUserRow += '<tr>\n' +
    		 
           '<td class="text-center">\n'
      	 + (i+1) +
           '</td>\n' +
           
           '<td>\n'
           + item.typelogement.libelleTypeLogement+
           '</td>\n' +
           
           '<td>\n'
           + item.libelleLogement+
           '</td>\n' +
           
           '<td>\n'
           + item.quartier.libelleQuartier +
           '</td>\n' +
           
           '<td>\n'
           + item.addresseLogement+
           '</td>\n' +
           
           
           '<td>\n'
           + item.superficieLogement+
           '</td>\n' +
           
           '<td>\n'
           + item.nombredepieces+
           '</td>\n' +
         
        
           
           '<td class="text-center">\n';   		           
          
    		 
    		 
    		 
    		dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="modal" data-target="#modal_large" onclick=ModifierLogement('+item.idLogement+') title="Modifier cette fourniture"><span class="glyphicon glyphicon-pencil"></span></button>';
      	   dListUserRow +='<button class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="modal" data-target="#modal_large" onclick=SupprimerLogement('+item.idLogement+') title="Supprimer cette fourniture"><span class="fa fa-trash"></span></button>';
            
    		 
    		 
    		 
           dListUserRow +=  actions+
          '</td>\n' 
           
          + '</tr>\n';
    		 
       }); 
    	 
    	 dListUser += dListUserRow;
       dListUser += ' </tbody><thead><tr>' +
       '<th width="5%"   class="text-center">Numero </th>' +
       '<th width="15%"  class="text-center">Type</th>' +
       '<th width="15%"  class="text-center">Libell&eacute; </th>' +
       '<th width="10%"  class="text-center">Quartier </th>' +
       '<th width="15%"  class="text-center">Adresse</th>' +
       '<th width="20%"  class="text-center">Superficie</th>' +
       '<th width="10%"  class="text-center">Nombre de pi&egrave;ce</th>' +
       '<th width="10%"  class="text-center">Actions</th>' +
       '</tr></thead></table>';
       
       
       
       
       $('#listeDemande').empty();
       $('#listeDemande').append(dListUser);
   
       
       var tableCourse = $('#datatable-fixed-header').dataTable({        	 
      	           
      	 
       	 //gerer la langue
	       "oLanguage": {
	            "sLengthMenu": "_MENU_ / pages",
	            "sZeroRecords": "Aucun resultat trouv&eacute;",
	            "sInfo": "_START_ a _END_ sur _TOTAL_ logement(s)",
	            "sInfoEmpty": "De 0 a 0 sur 0 client(s)",
	            "sInfoFiltered": "(total de _MAX_  logement(s))",
	            "sSearch": "Recherche:",
	            "oPaginate": {
	                "sFirst":    "Prem",
	                "sLast":    "Dern",
	                "sNext":    "Suiv",
	                "sPrevious": "Pr&eacute;c"
	            }
	        },

	       "bFilter": true, //enlever la recherche automatique
         "bSort": false,    //enlever les filtre
         "bLengthChange": true,//enlever la selection de plusieurs enregistrements
         "aLengthMenu": [[10,25, 50, 75, -1], [25, 50, 75, "Tout"]] //mettre le nombre d'enregitrement afficher par page
       });
    	 
     },
		error: function (data, status, er) {
			 alert("Insertion échouée"+status)	;
	    }
	});
}


function SupprimerLogement(idLogement){
	
	
	
	 swal({
	        title: "Voulez-vous vraiment supprimer ce logement ?",
	        text: "Cliquez sur valider pour confirmer.",
	        type: "warning",
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "Valider",
	        cancelButtonText: "Annuler",
	        showCancelButton: true,
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true
	    },
	    function () {
	       setTimeout(function () {
	
	 var key="deleteLogement";
	  var DTO={"action":key, 'idLogement':idLogement}
	
 $.ajax({      
		 
		 type: "POST",
	        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
	        url:ROOT_URL+"/LogementModuleServlet",
	        dataType: "json",
	        data: DTO,
       success: function (r) {
    	       	
    	   swal("Succès!", "Logément supprimé avec succès!", "success");
           document.location.reload(true);
    	  
    	
    		  
         },
 		error: function (data, status, er) {
 			 alert("Suppression échouée"+status)	;
 	    }
 	});
	
	       }, null);
	    });

	    return true;
}


$(document).ready(function () {
		
	getListLogement();
	
	
});