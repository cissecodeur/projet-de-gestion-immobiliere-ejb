package net.Immo.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import net.immo.entities.Logement;
import net.immo.entities.TypeClient;
import net.immo.interfaces.TypeClientI;

/**
 * Servlet implementation class TypeClientModuleServlet
 */
@WebServlet("/TypeClientModuleServlet")
public class TypeClientModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	//Injection de dependances
		
		@EJB
		private TypeClientI MetierTypeClient;
		
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TypeClientModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 String action=request.getParameter("action");
		  String json="";
		  
		  
		     if(action !=null){
		    	 
		    	 if(action.equals("addNewtypeClient")){   
		    		     		 
		    		 
		    		 String libelleTypeClient=request.getParameter("libelleTypeClient");
		   
		    		 
		    		 TypeClient tc= new TypeClient();
		    			    		
		    		 tc.setLibelleTypeClient(libelleTypeClient);
		    			    		
		    		 TypeClient retour= MetierTypeClient.addTypeClient(tc);
		    		
		    		json =new Gson().toJson(retour);
		    	 }
		    	 else if(action.equals("getListTypeClient")){   
		    		 
		    		 List<TypeClient> retour= MetierTypeClient.listTypeClient();
		    		 json =new Gson().toJson(retour);
		    		 
		         }
		    	 
		    	 else if(action.equals("deleteTypeClient")){   
		     		 
		        	 int idTypeClient=Integer.parseInt(request.getParameter("idTypeClient"));
		        	 
		        	 TypeClient retour=MetierTypeClient.SupprimerTypeClient(idTypeClient);
		        	 
		    		 json =new Gson().toJson(retour);
		         }
		     }
		     
		     response.setContentType("application/json");
		     response.getWriter().write(json);
				
		    		 
		
	}

}
