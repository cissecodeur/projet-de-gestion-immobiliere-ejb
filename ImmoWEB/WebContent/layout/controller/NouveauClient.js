var ROOT_URL='http://localhost:8080/ImmoWEB';

function formulaireNouveauClient(){
	
	var dDiv='';
	 dDiv +='<form id="demo-form" data-parsley-validate onsubmit ="return false">'+
     
	'<div class="row">'+
	'<div class="col-md-6">'+
		'<label for="heard">Type :</label>'+
		' <select  class="form-control" id="idTypeClient">'+
		' <option value="">Choose..</option>'+
		' <option value="1">Press</option>'+
		' <option value="2">Internet</option>'+
		' <option value="3">Word of mouth</option>'+
		'</select>'+
	'</div>'+
	'</div>'+
	
	
	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Raison Sociale :</label>'+
	'  <input type="text" class="form-control" id="RaisonSocialClient" />'+
	'</div>   	'+
	'<div class="col-md-6">'+
	'  <label for="fullname">Numero de Compte:</label>'+
	'  <input type="text" class="form-control" id="NumCompteContribuableClient" />'+
	'</div>'+
	'</div>'+
	'</div>'+
	
  '<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Nom :</label>'+
	'   <input class="form-control"  id="nomClient"/>'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label >Prenoms:</label>'+
	'   <input class="form-control"  id="prenomClient"/>'+
	'</div>'+	
	'</div>'+
	'</div>'+
	

	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Contact :</label>'+
	'   <input class="form-control"  id="contactClient"/>'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Addresse :</label>'+
	'   <input type="text"  class="form-control" id="addresseClient" />'+
	'</div>'+
	'</div>'+
	'</div>'+
	
	'<div class="row">'+
	'<div class="col-md-12">'+
	'  <label for="fullname">Activit&eacute; :</label>'+
	'  <input type="text" class="form-control" id="ActiviteClient" />'+
	'</div>'+
	'</div>'+

  	
  	
  	                           
	' <div class="col-md-6">'+
	'     <span class="btn btn-primary" onclick="addNewClient()">Valider</span>'+
	' </div>'+


	'</form>';
     
	$('#FormulaireNouveauClient').empty();
	$('#FormulaireNouveauClient').append(dDiv);
}





function addNewClient(){

	
	var idTypeClient = $("#idTypeClient").val();
    var nomClient= $("#nomClient").val();
    var prenomClient= $("#prenomClient").val();
    var contactClient= $("#contactClient").val();
    var addresseClient= $("#addresseClient").val();
    var ActiviteClient= $("#ActiviteClient").val();
    var RaisonSocialClient= $("#RaisonSocialClient").val();
    var NumCompteContribuableClient= $("#NumCompteContribuableClient").val();
    
    
    swal({
        title: "Voulez-vous vraiment ajouter ce client ?",
        text: "Cliquez sur valider pour confirmer.",
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function () {
       setTimeout(function () {
    	
    var key="addNewClient";
	var DTO={"action":key, "idTypeClient":idTypeClient,"nomClient":nomClient,"prenomClient":prenomClient,"contactClient":contactClient,"addresseClient":addresseClient,"ActiviteClient":ActiviteClient,"RaisonSocialClient":RaisonSocialClient,"NumCompteContribuableClient":NumCompteContribuableClient}
	
    
// Bloc ajax
	
    $.ajax({

        type: "POST",
        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        url:ROOT_URL+"/ClientModuleServlet",
        dataType: "json",
        data: DTO,
        
        success: function (r) { 
        	           
           
        	if(r.idClient>0){		          
   	    	 
                swal("Succès!", "Client ajouté avec succès!", "success");
                document.location.reload(true);

            }else{
                swal("Erreur...", "Impossible d'ajouter ce client!\n Actualiser la page puis reessayer!!! ", "error");
            }
        	
        },
        error: function (r) {
        	 swal("Erreur...", "Impossible d'executer la requette \n  Erreur: "+ r, "error");
        }
    });

       }, null);
    });

    return true;
    }



$(document).ready(function () {
	
	formulaireNouveauClient();
	
	
});