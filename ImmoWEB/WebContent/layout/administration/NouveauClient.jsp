
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Gestion immobiliere | </title>

  <!-- Bootstrap core CSS -->

  <link href="<%=request.getContextPath() %>/resources/css/bootstrap.min.css" rel="stylesheet">

  <link href="<%=request.getContextPath() %>/resources/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<%=request.getContextPath() %>/resources/css/animate.min.css" rel="stylesheet">



 <!--sweet alert-->
     <script type="text/javascript" src="<%=request.getContextPath() %>/resources/js/sweet_alert/sweetalert-dev.js"></script>          	    
	 <link rel="stylesheet" type="text/css" id="theme" href="<%=request.getContextPath() %>/resources/js/sweet_alert/sweetalert.css"/>
       


  <!-- Custom styling plus plugins -->
  <link href="<%=request.getContextPath() %>/resources/css/custom.css" rel="stylesheet">
  <link href="<%=request.getContextPath() %>/resources/css/icheck/flat/green.css" rel="stylesheet">
  <!-- editor -->
  <link href="<%=request.getContextPath() %>/resources/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
  <link href="<%=request.getContextPath() %>/resources/css/editor/index.css" rel="stylesheet">
  <!-- select2 -->
  <link href="<%=request.getContextPath() %>/resources/css/select/select2.min.css" rel="stylesheet">
  <!-- switchery -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/switchery/switchery.min.css" />

  <script src="<%=request.getContextPath() %>/resources/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<%=request.getContextPath() %>/layout/dashboard/index.jsp" class="site_title"><i class="fa fa-paw"></i> <span>Immobilier</span></a>
          </div>
          <div class="clearfix"></div>


          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="<%=request.getContextPath() %>/resources/images/codeur.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Bienvenue,</span>
              <h2>Codeur 91</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />




          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
   				<jsp:include page="../include/sidebar.jsp"/>
          </div>
          <!-- /sidebar menu -->





          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<%=request.getContextPath() %>/resources/images/codeur.jpg" alt="">Codeur 91
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="javascript:;">  Profil</a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <span class="badge bg-red pull-right">50%</span>
                      <span>Parametres</span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">Aide</a>
                  </li>
                  <li><a href="<%=request.getContextPath() %>/login.jsp"><i class="fa fa-sign-out pull-right"></i> Deconnexion</a>
                  </li>
                </ul>
              </li>

              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<%=request.getContextPath() %>/resources/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<%=request.getContextPath() %>/resources/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<%=request.getContextPath() %>/resources/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<%=request.getContextPath() %>/resources/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <div class="text-center">
                      <a>
                        <strong>Voir toutes les alertes</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>

      </div>
      
      
      
      
      
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">                 


          <div class="row">          
                           
              <div class="x_panel">
                <div class="x_title">
                  <h2>Nouveau client</small></h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <!-- start form for validation -->                                  
                 
                  <div  id="FormulaireNouveauClient" > </div>                     
                  
                  <!-- end form for validations -->

                </div>
              </div>         

        </div>         
                 

          
     </div>
  </div>
      <!-- /page content -->






      <!-- footer content -->
      <footer>
        <div class="pull-right">
         <a href="https://colorlib.com">Codeur 91</a>
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
      
      
      
    </div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="<%=request.getContextPath() %>/resources/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<%=request.getContextPath() %>/resources/js/progressbar/bootstrap-progressbar.min.js"></script>
  
  <!-- icheck -->
  <script src="<%=request.getContextPath() %>/resources/js/icheck/icheck.min.js"></script>
  <!-- tags -->
  <script src="<%=request.getContextPath() %>/resources/js/tags/jquery.tagsinput.min.js"></script>
  <!-- switchery -->
  <script src="<%=request.getContextPath() %>/resources/js/switchery/switchery.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<%=request.getContextPath() %>/resources/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath() %>/resources/js/datepicker/daterangepicker.js"></script>
  <!-- richtext editor -->
  <script src="<%=request.getContextPath() %>/resources/js/editor/bootstrap-wysiwyg.js"></script>
  <script src="<%=request.getContextPath() %>/resources/js/editor/external/jquery.hotkeys.js"></script>
  <script src="<%=request.getContextPath() %>/resources/js/editor/external/google-code-prettify/prettify.js"></script>
  <!-- select2 -->
  <script src="<%=request.getContextPath() %>/resources/js/select/select2.full.js"></script>
  <!-- form validation -->
  <script type="text/javascript" src="<%=request.getContextPath() %>/resources/js/parsley/parsley.min.js"></script>
  <!-- textarea resize -->
  <script src="<%=request.getContextPath() %>/resources/js/textarea/autosize.min.js"></script>
  <script>
    autosize($('.resizable_textarea'));
  </script>
  <!-- Autocomplete -->
  <script type="text/javascript" src="<%=request.getContextPath() %>/resources/js/autocomplete/countries.js"></script>
  <script src="<%=request.getContextPath() %>/resources/js/autocomplete/jquery.autocomplete.js"></script>
  <!-- pace -->
  <script src="<%=request.getContextPath() %>/resources/js/pace/pace.min.js"></script>
  
  
    <script src="<%=request.getContextPath() %>/layout/controller/NouveauClient.js"></script>
      
  
  
  
  
  
  <script type="text/javascript">
    $(function() {
      'use strict';
      var countriesArray = $.map(countries, function(value, key) {
        return {
          value: value,
          data: key
        };
      });
      // Initialize autocomplete with custom appendTo:
      $('#autocomplete-custom-append').autocomplete({
        lookup: countriesArray,
        appendTo: '#autocomplete-container'
      });
    });
  </script>
  <script src="<%=request.getContextPath() %>/resources/js/custom.js"></script>


</body>

</html>
