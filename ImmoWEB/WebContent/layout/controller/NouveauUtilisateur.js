var ROOT_URL='http://localhost:8080/ImmoWEB';

function formulaireNouveauUtilisateur(){
	
	var dDiv='';
	 dDiv +='<form id="demo-form" data-parsley-validate onsubmit ="return false">'+
     
'<div class="row">'+	
	'<div class="form-group">'+
	'<div class="col-md-4">'+
		'<label for="heard">Compte :</label>'+
		' <select  class="form-control" id="idCompte">'+
		' <option value="">Choose..</option>'+
		' <option value="1">Press</option>'+
		' <option value="2">Internet</option>'+
		' <option value="3">Word of mouth</option>'+
		'</select>'+
	'</div>'+
		
	'<div class="col-md-4">'+
		'<label for="heard">Profil :</label>'+
		' <select  class="form-control" id="idProfil">'+
		' <option value="">Choose..</option>'+
		' <option value="1">Press</option>'+
		' <option value="2">Internet</option>'+
		' <option value="3">Word of mouth</option>'+
		'</select>'+
	'</div>'+
			
	'<div class="col-md-4">'+
		'<label for="heard">Groupe :</label>'+
		' <select  class="form-control" id="idGroupe">'+
		' <option value="">Choose..</option>'+
		' <option value="1">Press</option>'+
		' <option value="2">Internet</option>'+
		' <option value="3">Word of mouth</option>'+
		'</select>'+
	'</div>'+
 '</div>'+
'<div>'+	
	
	
	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Nom :</label>'+
	'  <input type="text" class="form-control" id="nomUtilisateur" />'+
	'</div>   	'+
	'<div class="col-md-6">'+
	'  <label for="fullname">Prenom :</label>'+
	'  <input type="text" class="form-control" id="prenomUtilisateur" />'+
	'</div>'+
	'</div>'+
'</div>'+
	
  '<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Email:</label>'+
	'   <input class="form-control"  id="emailUtilisateur"/>'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label >Telephone:</label>'+
	'   <input class="form-control"  id="telephoneUtilisateur"/>'+
	'</div>'+	
	'</div>'+
	'</div>'+
	

	'<div class="row">'+
	'<div class="form-group">'+
	
	'<div class="col-md-6">'+
	'  <label >Fonction :</label>'+
	'   <input class="form-control"  id="fonctionUtlisateur"/>'+
	'</div>'+
	
	'<div class="col-md-6">'+
	'  <label for="fullname">Mot de passe :</label>'+
	'   <input type="text"  class="form-control" id="passwordUtlisateur" />'+
	'</div>'+
	'</div>'+
	'</div>'+
	
	'<div class="row">'+
	'<div class="col-md-2">'+
	'<label for="heard">Etat :</label>'+
	' <select  class="form-control" id="etatUtilisateur">'+
	' <option value="">Choose..</option>'+
	' <option value="1">activ&eacute;</option>'+
	' <option value="2">desactiv&eacute;</option>'+
	'</select>'+
'</div>'+
'</div>'+

	'<div>'+
	'</div>'+
	'<div>'+
	'</div>'+
	'<div>'+
	'</div>'+
  	
'<div class="row">'+
  	                           
	' <div class="col-md-6">'+
	'     <span class="btn btn-primary" onclick="addNewUtilisateur()">Valider</span>'+
	' </div>'+
'<div>'+

	'</form>';
     
	$('#FormulaireNouveauUtilisateur').empty();
	$('#FormulaireNouveauUtilisateur').append(dDiv);
}





function addNewUtilisateur(){

	
	var idCompte = $("#idCompte").val();
	var idProfil = $("#idProfil").val();
	var idGroupe = $("#idGroupe").val();
    var nomUtilisateur= $("#nomUtilisateur").val();
    var prenomUtilisateur= $("#prenomUtilisateur").val();
    var emailUtilisateur= $("#emailUtilisateur").val();
    var telephoneUtilisateur= $("#telephoneUtilisateur").val();
    var fonctionUtlisateur= $("#fonctionUtlisateur").val();
    var passwordUtilisateur= $("#passwordUtilisateur").val();
    var NumCompteContribuableClient= $("#etatUtilisateur").val();
    
    
    swal({
        title: "Voulez-vous vraiment ajouter ce client ?",
        text: "Cliquez sur valider pour confirmer.",
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function () {
       setTimeout(function () {
    	
    var key="addNewClient";
	var DTO={"action":key, "idCompte":idCompte,"idProfil":idProfil,"idGroupe":idGroupe,"nomUtilisateur":nomUtilisateur,"prenomUtilisateur":prenomUtilisateur,"emailUtilisateur":emailUtilisateur,"telephoneUtilisateur":telephoneUtilisateur,"fonctionUtlisateur":fonctionUtlisateur,"passwordUtilisateur":passwordUtilisateur,"etatUtilisateur":etatUtilisateur}
	
    
// Bloc ajax
	
    $.ajax({

        type: "POST",
        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        url:ROOT_URL+"/UtilisateurModuleServlet",
        dataType: "json",
        data: DTO,
        
        success: function (r) { 
        	           
           
        	if(r.idClient>0){		          
   	    	 
                swal("Succès!", "Client ajouté avec succès!", "success");
                document.location.reload(true);

            }else{
                swal("Erreur...", "Impossible d'ajouter ce utilisateur!\n Actualiser la page puis reessayer!!! ", "error");
            }
        	
        },
        error: function (r) {
        	 swal("Erreur...", "Impossible d'executer la requette \n  Erreur: "+ r, "error");
        }
    });

       }, null);
    });

    return true;
    }



$(document).ready(function () {
	
	formulaireNouveauUtilisateur();
	
	
});