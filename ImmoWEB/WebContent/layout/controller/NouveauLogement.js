var ROOT_URL='http://localhost:8080/ImmoWEB';


function formulaireNouveauLogement(){
	
	var dDiv = '';
	
	
	dDiv +='<form id="demo-form" data-parsley-validate onsubmit ="return false">'+
     
     
	'<div class="col-md-6">'+
		'<label for="heard">Type :</label>'+
		' <select  class="form-control" id="idTypeLogement">'+
		' <option value="">Choose..</option>'+
		' <option value="1">villa</option>'+
		' <option value="2">duplex</option>'+
		' <option value="3">bn</option>'+
		'</select>'+
	'</div>'+
           
	' <div class="col-md-6">'+
	'<label for="heard">Libell&eacute; :</label>'+
	'     <select class="form-control" id="libelleLogement">'+
	'        <option value="">Choose..</option>'+
	'         <option value="press">Press</option>'+
	'         <option value="net">Internet</option>'+
	'         <option value="mouth">Word of mouth</option>'+
	'       </select>'+
	' </div>'+
         
	
	
	' <div class="col-md-6">'+
	'<label for="heard">Quartier :</label>'+
	'     <select class="form-control" id="idQuartier">'+
	'        <option value="0">Choose..</option>'+
	'         <option value="1">Paillet</option>'+
	'         <option value="2">Willy	</option>'+
	'         <option value="3">Dokui</option>'+
	'       </select>'+
	' </div>'+

	'<div class="col-md-12">'+
	'  <label >Adresse :</label>'+
	'   <input class="form-control"  id="addresseLogement"/>'+
	'</div>'+
	
	'<div class="col-md-12">'+
	'  <label for="fullname">Superficie :</label>'+
	'   <input type="text"  class="form-control" id="Superficie" />'+
	'</div>'+

	'<div class="col-md-12">'+
	'  <label for="fullname">Nombre de pi&egrave;ce :</label>'+
	'  <input type="text" class="form-control" id="nombredePiece" />'+
	'</div>   	'+
  	
  	
  	                           
	' <div class="col-md-12">'+
	'     <span class="btn btn-primary" onclick="addNewLogement()">Valider</span>'+
	' </div>'+


	'</form>';
     
	$('#FormulaireNouveauLogement').empty();
	$('#FormulaireNouveauLogement').append(dDiv);
}




function addNewLogement(){

	
	var idTypeLogement = $("#idTypeLogement").val();
	var libelleLogement= $("#libelleLogement").val();
	var idQuartier= $("#idQuartier").val();
	var addresseLogement= $("#addresseLogement").val();
	var Superficie= $("#Superficie").val();
	var nombredePiece= $("#nombredePiece").val();

    
	

    swal({
        title: "Voulez-vous vraiment ajouter ce logement ?",
        text: "Cliquez sur valider pour confirmer.",
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Valider",
        cancelButtonText: "Annuler",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function () {
       setTimeout(function () {
    	
    var key="addNewLogement";
var DTO={"action":key, "idTypeLogement":idTypeLogement,"libelleLogement":libelleLogement,"idQuartier":idQuartier,"addresseLogement":addresseLogement,"Superficie":Superficie,"nombredePiece":nombredePiece}
	
    
// Bloc ajax
	
    $.ajax({

        type: "POST",
        contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        url:ROOT_URL+"/LogementModuleServlet",
        dataType: "json",
        data: DTO,
        
        success: function (r) { 
        	           
           
        	if(r.idLogement>0){		          
   	    	 
                swal("Succès!", "Logement ajouté avec succès!", "success");
                document.location.reload(true);

            }else{
                swal("Erreur...", "Impossible d'ajouter ce logement!\n Actualiser la page puis reessayer!!! ", "error");
            }
        	
        },
        error: function (r) {
        	 swal("Erreur...", "Impossible d'executer la requette \n  Erreur: "+ r, "error");
        }
    });

       }, null);
    });

    return true;
	
	
    
 }




$(document).ready(function () {
		
	formulaireNouveauLogement();
	
	
});